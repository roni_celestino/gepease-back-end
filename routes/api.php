<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/eventos', [
    'uses' => 'EventosController@index',
]);

Route::get('/evento/{id}', [
    'uses' => 'EventosController@show',
]);

Route::get('/programacao/{eventos_id}', [
    'uses' => 'ProgramacaoController@getProgramacao',
]);

Route::get('/comissoes/{eventos_id}', [
    'uses' => 'ComissoesController@getComissoes',
]);

Route::get('/certificados-download/{eventos_id}', [
    'uses' => 'CertificadosController@getCertificados',
]);

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();
}); */
