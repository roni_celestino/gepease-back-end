<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trabalhosDetalhes extends Model
{
    protected $fillable = [
        'nome',
        'resumo'
    ];

    public function trabalhos()
    {
        return $this->belongsToMany(trabalhos::class);
    }
}
