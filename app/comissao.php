<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comissao extends Model
{

    protected $table = 'comissoes';

    protected $fillable = [
        'titulo',
        'descricao',
    ];

    public function comissaoIntegrantes()
    {
        return $this->hasMany('App\ComissaoIntegrantes', 'comissao_id');
    }
}

class ComissaoIntegrantes extends Model
{
    protected $table = 'comissao_integrantes';
    protected $fillable = [
        'nome',
    ];

}
