<?php

namespace GEPEASE;

use Illuminate\Database\Eloquent\Model;

class SendMailFillable extends Model
{
    protected $fillable = [     
            'nome',
            'email',
            'telefone',
            'mensagem'       
    ];
}
