<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trabalhos extends Model
{
    protected $fillable = [
        'autores'
    ];

    public function eventos()
    {
        return $this->belongsToMany(eventos::class);
    }
}
