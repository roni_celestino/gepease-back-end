<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\sendMail;
use Mail;

class SendMailController extends Controller
{
    /**
     * Show the application sendMail.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendMail(Request $request)
    {
        $camposEmail = array(
            'nome' => $request->get('nome'),
            'email' => $request->get('email'),
            'telefone' => $request->get('telefone'),
            'mensagem' => $request->get('mensagem')
        );
        $receiverAddress = 'sextoesea@gmail.com';
        Mail::to($receiverAddress)->send(new sendMail($camposEmail));
        return response()->json($camposEmail, 200);
    }
}
