<?php
namespace App\Http\Controllers;

use App\Professores;
use JWTAuth;
use Illuminate\Http\Request;

class ProfessoresController extends Controller
{
    
    public function getProfessores()
    {
        $professores = Professores::all();
        $response = [
            'professores' => $professores
        ];
        return response()->json($response, 200);
    }

    public function postProfessor(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();
        $professor = new professor();
        $professor = $professor -> create($request->all());
        $professor->save();
        return $professor;
    }
}
