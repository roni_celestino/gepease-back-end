<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Ouvintes;

class OuvintesController extends Controller
{
    public function getOuvintes()
    {
        $ouvintes = Ouvintes::all();
        $response = [
          'ouvintes' => $ouvintes
        ];
        return response()->json($response, 200);
    }

    public function postOuvinte(Request $request)
    {
        $ouvinte = new ouvintes();
        $ouvinte = $ouvinte -> create($request->all());
        $ouvinte->save();
        return $ouvinte;
    }
}
