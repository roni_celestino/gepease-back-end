<?php

namespace App\Http\Controllers;

use App\Programacao;
use App\ProgramacaoCoordenadores;
use App\ProgramacaoDias;
use App\ProgramacaoMinistrantes;
use Illuminate\Http\Request;

class ProgramacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getProgramacao(Request $request, $eventos_id)
    {
        //$programacoes = Programacao::with('ProgramacaoCoordenadores', 'ProgramacaoMinistrantes')
        $programacoesDoDia = ProgramacaoDias::where('eventos_id', '=', $eventos_id)
            ->with('Programacao')
            ->orderBy('data', 'asc')
            ->get();

        foreach ($programacoesDoDia as $programacaoDoDia) {
            $programacao = $programacaoDoDia->programacao;
            foreach ($programacao as $programacaoo) {
                $programacaoCoordenadores = $programacaoo->ProgramacaoCoordenadores;
                foreach ($programacaoCoordenadores as $programacaoCoordenadoress) {
                }
                $programacaoMinistrantes = $programacaoo->ProgramacaoMinistrantes;
                foreach ($programacaoMinistrantes as $programacaoMinistrantess) {
                }
            }
        }

        if (!$programacoesDoDia) {
            return response()->json(['message' => 'Document not found'], 404);
        }

        return response()->json(
            $programacoesDoDia,
            200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
