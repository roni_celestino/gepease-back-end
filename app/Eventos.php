<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/* EVENTO */
class Eventos extends Model
{
    protected $table = 'eventos';
    protected $fillable = [
        'titulo',
        'apresentacao',
        'data',
    ];
}

/* PROGRAMAÇÃO DIA */
class ProgramacaoDias extends Model
{
    protected $table = 'programacao_dias';
    protected $fillable = [
        'id',
        'eventos_id',
        'data',
    ];
    
    public function Programacao()
    {
        return $this->hasMany('App\Programacao', 'programacao_dias_id');
    }
}

/* PROGRAMACAO */
class Programacao extends Model
{
    protected $table = 'programacoes';
    protected $fillable = [
        'id',
        'eventos_id',
        'programacao_dias_id',
        'titulo',
        'tema',
        'objetivo',
        'detalhes',
        'resumo',
        'requisitos',
        'data_inicio',
        'data_final',
        'hora_inicio',
        'hora_final',
        'duracao',
        'vagas',
        'local',
        'eventos_id'
    ];

    public function ProgramacaoCoordenadores()
    {
        return $this->hasMany('App\programacaoCoordenadores', 'programacao_id');
    }

    public function ProgramacaoMinistrantes()
    {
        return $this->hasMany('App\ProgramacaoMinistrantes', 'programacao_id');
    }
}

/* PROGRAMACAO COORDENADORES */
class ProgramacaoCoordenadores extends Model
{
    protected $table = 'programacao_coordenadores';
    protected $fillable = [
        'nome',
        'programacao_id',
    ];
}

/* PROGRAMACAO MINISTRANTES */

class ProgramacaoMinistrantes extends Model
{
    protected $table = 'programacao_ministrantes';
    protected $fillable = [
        'nome',
        'programacao_id',
    ];
}
