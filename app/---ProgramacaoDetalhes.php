<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramacaoDetalhes extends Model
{
    protected $table = 'programacao_detalhes';

    protected $fillable = [
        'titulo',
        'tema',
        'objetivo',
        'detalhes',
        'resumo',
        'requisitos',
        'data_inicio',
        'data_final',
        'hora_inicio',
        'hora_final',
        'duracao',
        'vagas',
        'local',
    ];
    public function Programacao()
    {
        return $this->hasMany('App\Programacao');

    }
}
