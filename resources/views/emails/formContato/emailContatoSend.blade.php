@component('mail::layout')
{{-- Header --}}
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
    <a href="http://gepease.com.br" target="_blank"> <img src="http://gepease.com.br/assets/logo/logo-header.png" style="height:120px" height="120" /></a>
    @endcomponent
@endslot

{{-- Body --}}
<p style="text-align:center !important;">
Mensagem enviada pelo formulário de contato do site
 <a href="http://gepease.com.br" target="_blank">www.gepease.com.br</a>
</p>
<br>

<strong>Nome: </strong>{{ $camposEmail['nome'] }} <br>

<strong>Email: </strong>{{ $camposEmail['email'] }} &nbsp; &nbsp; &nbsp; <strong>Telefone:</strong> {{ $camposEmail['telefone'] }}<br>

<strong>Mensagem:</strong><br>
{{ $camposEmail['mensagem'] }} 


{{-- Footer --}}
@slot('footer')
    @component('mail::footer')
        &copy; {{ date('Y') }} {{ config('app.name') }}. Todos os direitos reservados.
    @endcomponent
@endslot
@endcomponent


