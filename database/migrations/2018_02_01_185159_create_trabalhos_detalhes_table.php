<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrabalhosDetalhesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabalhos_detalhes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('trabalhos_id')->unsigned();

            $table->foreign('trabalhos_id')
                ->references('id')
                ->on('trabalhos')
                ->onDelete('cascade');

            $table->longText('nome');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trabalhos_detalhes');
    }
}
