<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramacaoCoordenadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programacao_coordenadores', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('programacao_id')->unsigned();

            $table->foreign('programacao_id')
                ->references('id')
                ->on('programacoes')
                ->onDelete('cascade');

            $table->integer('eventos_id')->unsigned();
            $table->foreign('eventos_id')
                ->references('id')
                ->on('eventos')
                ->onDelete('cascade');

            $table->string('nome');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programacao_coordenadores');
    }
}
