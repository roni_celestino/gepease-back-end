<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programacoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('eventos_id')->unsigned();
            $table->foreign('eventos_id')
                ->references('id')
                ->on('eventos')
                ->onDelete('cascade');

                $table->integer('programacao_dias_id')->unsigned();
                $table->foreign('programacao_dias_id')
                    ->references('id')
                    ->on('programacao_dias')
                    ->onDelete('cascade');


            $table->longText('titulo');

            $table->longText('tema');

            $table->longText('objetivo')->nullable();

            $table->longText('detalhes')->nullable();

            $table->longText('resumo')->nullable();

            $table->longText('requisitos')->nullable();

            $table->string('data_inicio');

            $table->string('data_final');

            $table->string('hora_inicio');

            $table->string('hora_final');

            $table->string('duracao')->nullable();

            $table->string('vagas')->nullable();

            $table->string('local');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programacoes');
    }
}
