<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComissaoIntegrantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comissao_integrantes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('comissao_id')->unsigned();

            $table->foreign('comissao_id')
                ->references('id')
                ->on('comissao')
                ->onDelete('cascade');

            $table->longText('nome');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comissao_integrantes');
    }
}
