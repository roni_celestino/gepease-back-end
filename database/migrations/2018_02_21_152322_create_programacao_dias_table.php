<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramacaoDiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programacao_dias', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('eventos_id')->unsigned();
            $table->foreign('eventos_id')
                ->references('id')
                ->on('eventos')
                ->onDelete('cascade');
                
            $table->string('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programacao_dias');
    }
}
